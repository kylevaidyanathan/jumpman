// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "JumpmanPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class JUMPMAN_API AJumpmanPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AJumpmanPlayerController();

protected:

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void MoveForward(float Value);
	void MoveRight(float Value);

	void Climb(float Value);

	ACharacter * PlayerCharacter;
};
