// Fill out your copyright notice in the Description page of Project Settings.

#include "Jumpman.h"
#include "JumpmanPlayerController.h"

AJumpmanPlayerController::AJumpmanPlayerController()
{
	// get character from pawn
	auto character = GetPawn();
	PlayerCharacter = Cast<ACharacter>(character);
}

void AJumpmanPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AJumpmanPlayerController::SetupInputComponent()
{
	// Set up gameplay key bindings
	Super::SetupInputComponent();
	//InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	//InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAxis("MoveForward", this, &AJumpmanPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AJumpmanPlayerController::MoveRight);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &AJumpmanPlayerController::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &AJumpmanPlayerController::TouchStopped);
}

void AJumpmanPlayerController::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		PlayerCharacter->Jump();
	}
}

void AJumpmanPlayerController::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		PlayerCharacter->StopJumping();
	}
}

void AJumpmanPlayerController::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		APawn* Pawn = GetPawn();
		if (Pawn)
		{
			Pawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
		}
	}
}

void AJumpmanPlayerController::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		APawn* Pawn = GetPawn();
		if (Pawn)
		{
			Pawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
		}
	}
}

void AJumpmanPlayerController::Climb(float Value)
{

}
