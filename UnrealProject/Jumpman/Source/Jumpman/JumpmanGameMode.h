// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "JumpmanGameMode.generated.h"

UCLASS(minimalapi)
class AJumpmanGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AJumpmanGameMode();
};



