// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#ifndef JUMPMAN_JumpmanCharacter_generated_h
	#include "JumpmanCharacter.h"
#endif
#ifndef JUMPMAN_JumpmanGameMode_generated_h
	#include "JumpmanGameMode.h"
#endif
